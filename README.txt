This is a minimalistic-designed text editor, for making quick notes. 
It has several useful features, such as: auto-hiding, auto-saving, minimizing to tray and global hotkeys.