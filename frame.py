import sys

from PyQt5.QtCore import Qt, QTranslator, QSize, QPoint
from PyQt5.QtGui import QIcon, QPixmap, QFont, QTextOption, QKeySequence, QDesktopServices, QTextCursor, QTextCharFormat
from PyQt5.QtWidgets import QApplication, QMainWindow, QTabWidget, QWidget, QGridLayout, QSystemTrayIcon, QAction, QMenu, QShortcut, QLabel, QPlainTextEdit, QMessageBox, QAbstractSlider

import info
import res.sn_rc  # Resources
from tran.tr import tr  # Translator
import common

import tab_controller


class SNQMainWindow(QMainWindow):
    """ QMainWindow reimplementation """
    def __init__(self):
        QMainWindow.__init__(self)
        self.xc = common.Capacitor()
        self.timer = None

    def resizeEvent(self, ev):
        super(self.__class__, self).resizeEvent(ev)
        self.xc.dbg_show_stats()
        self.xc.save_geometry()

    def moveEvent(self, ev):
        super(self.__class__, self).moveEvent(ev)
        self.xc.dbg_show_stats()
        self.xc.save_geometry()

    def timerEvent(self, ev):
        super(self.__class__, self).timerEvent(ev)
        self.xc.save()


# class SNHighlighter(QSyntaxHighlighter):
#     """ This will help colorizing text.. sometimes"""
#     def __init__(self, *args, compRegExp):
#         QSyntaxHighlighter.__init__(self, *args)
#         self.compRegExp = compRegExp
#
#     def highlightBlock(self, text):
#         print('CHECKING : [{0}]'.format(text))
#         myClassFormat = QTextCharFormat()
#         myClassFormat.setForeground(Qt.blue)
#         myClassFormat.setFontUnderline(True)
#         for m in self.compRegExp.finditer(text):
#             self.setFormat(m.start(), m.end() - m.start(), myClassFormat)


class Mainframe(object):
    """ Main object """
    def __init__(self):
        # Configuration
        self.sets, self.opts = common.make_opts()
        self.quiting = False

        # self.setup_event_dispatcher()
        self.translator_ru = self.setup_translator()
        self.font_main = self.setup_font()

        self.app = self.setup_app(sys.argv)

        self.mwin = self.setup_main_window()

        self.local_hotkeys = self.setup_local_hotkeys()

        self.actions = self.setup_actions()
        self.main_cntx_menu = self.setup_context_menu()

        self.tray_icon = self.setup_tray_icon()

        self.wdg_main = self.setup_main_widget()

        self.wdg_tab = self.setup_tab_widget()

        if self.sets.contains('Debug'):
            self.dbg = self.setup_connect_debug()

        self.tabctrl = self.setup_tabs_controller()

        self.wdg_main_lay = self.setup_main_layout()

        for key in self.tabctrl.get_keylist():
            self.connect_txtbox(self.tabctrl.get_txtbox(key))

        # Double triks
        #  Debug
        if self.sets.contains('Debug'):
            self.action_debug_trig()
            self.action_debug_trig()
            self.tabctrl.get_txtbox().cursorPositionChanged.connect(self.dbg_show_cur_pos)
        #  Language
        self.action_language_trig()
        self.action_language_trig()  # Setting the right title and retranslating

        # Show window
        self.m_show_active()
        for key in self.tabctrl.get_keylist():
            # making tab active for current-tab trigs (transforming hyperlinks for ex.)
            # TODO when *_trigs will be separated from *_sets, change to just parse hyperlinks after loading
            #  and move .setCurrentIndex and setFocus to setup_tabs_controller
            self.tabctrl.tabholder.setCurrentWidget(self.tabctrl.get_tab(key))
            # loading text and cursor position
            self.load(key)
            self.restore_cursor_position(key)
        # restoring the currently opened tab
        try:
            focused_tab_key = self.sets.value('Win/FocusedTabKey', type=str)
            focused_tab = self.tabctrl.get_tab(focused_tab_key)
        except tab_controller.SNTabsException as e:
            self.logprint('ERROR: {}'.format(e))
            self.tabctrl.tabholder.setCurrentIndex(0)
        else:
            self.tabctrl.tabholder.setCurrentWidget(focused_tab)
        self.tabctrl.get_txtbox().setFocus()

    # --------------------------------------
    # Setups, mains
    # --------------------------------------

    def setup_event_dispatcher(self):
        # self.evdisp = QAbstractEventDispatcher.instance()
        # self.lol = LolEater()
        # self.evdisp.installEventFilter(self.lol)
        # self.logprint('{0} {1}', type(self.evdisp), dir(self.evdisp))
        pass

    @staticmethod
    def setup_translator():
        translator_ru = QTranslator()
        translator_ru.load("tran_scavenotes_ru", "tran")
        return translator_ru

    @staticmethod
    def setup_font():
        return QFont('Verdana', 8)

    def setup_app(self, argv):
        # MAIN SCREEN TURN ON
        app = QApplication(argv)
        app.setOrganizationName(info.organization)
        app.setApplicationName('{0}'.format(info.program))
        # Сonnections
        app.focusChanged.connect(self.focuslot)
        # Settings
        app.setWindowIcon(QIcon(QPixmap(":/main/ico.png")))
        app.setFont(self.font_main)

        return app

    def setup_main_window(self):
        # Main window
        mwin = SNQMainWindow()
        # Geometry
        mwin.resize(self.sets.value('Win/Size', type=QSize))
        mwin.move(self.sets.value('Win/Pos', type=QPoint))
        # Titles
        mwin.setWindowIcon(self.app.windowIcon())
        mwin.setWindowTitle('{0} v{1}'.format(info.program, info.version))
        if self.sets.value('Win/AlwaysOnTop', type=str).upper() == 'TRUE':
            mwin.setWindowFlags(mwin.windowFlags() | Qt.WindowStaysOnTopHint)
        # self.mwin.setWindowFlags(self.mwin.windowFlags() & ~(Qt.WindowMaximizeButtonHint | Qt.WindowMinimizeButtonHint))
        # Functions for events
        mwin.xc.save = self.save
        mwin.xc.m_hide = self.m_hide
        mwin.xc.logprint = self.logprint
        mwin.xc.save_geometry = self.save_geometry
        if self.sets.contains('Debug'):
            mwin.xc.dbg_show_stats = self.dbg_show_stats
        else:
            mwin.xc.dbg_show_stats = common.EmptyObject.instance
        return mwin

    def setup_local_hotkeys(self):
        local_hotkeys = []
        # Hide windows
        hk_esc = QShortcut(QKeySequence("Esc"), self.mwin)
        hk_esc.activated.connect(self.m_hide)
        local_hotkeys.append(hk_esc)
        # Save current tab
        hk_ctrl_s = QShortcut(QKeySequence("Ctrl+S"), self.mwin)
        hk_ctrl_s.activated.connect(self.save)
        local_hotkeys.append(hk_ctrl_s)
        # Cycle through tabs
        hk_ctrl_tab = QShortcut(QKeySequence("Ctrl+Tab"), self.mwin)
        hk_ctrl_tab.activated.connect(self.tab_wheel)
        local_hotkeys.append(hk_ctrl_tab)
        # Quit
        if self.sets.contains('Debug'):
            hk_ctrl_q = QShortcut(QKeySequence("Ctrl+Q"), self.mwin)
            hk_ctrl_q.activated.connect(self.set_quiting)
            local_hotkeys.append(hk_ctrl_q)

        return local_hotkeys

    def setup_actions(self):
        # Actions Capacitor
        actions = common.Capacitor()
        # Hidden
        actions.hiddenToggle = QAction('Hidden', self.mwin)
        actions.hiddenToggle.setCheckable(True)
        actions.hiddenToggle.triggered.connect(self.action_hidden_trig)
        # Auto hide
        actions.autoHideToggle = QAction('Auto hide', self.mwin)
        actions.autoHideToggle.setCheckable(True)
        actions.autoHideToggle.setChecked(True if self.sets.value('Win/AutoHide', type=str).upper() == 'TRUE' else False)
        actions.autoHideToggle.triggered.connect(self.action_autohide_trig)
        # Always on top
        actions.alwaysOnTopToggle = QAction('Always on top', self.mwin)
        actions.alwaysOnTopToggle.setCheckable(True)
        actions.alwaysOnTopToggle.setChecked(True if self.sets.value('Win/AlwaysOnTop', type=str).upper() == 'TRUE' else False)
        actions.alwaysOnTopToggle.triggered.connect(self.action_alwaysontop_trig)
        # Word wrap
        actions.wordWrapToggle = QAction('Word wrap', self.mwin)
        # actions.wordWrapToggle.setCheckable(True)
        # actions.wordWrapToggle.setChecked(True if self.sets.value('Textbox/WordWrap', type=str).upper() == 'TRUE' else False)
        actions.wordWrapToggle.triggered.connect(self.action_wordwrap_trig)
        # Parse links
        actions.parseLinksToggle = QAction('Parse links', self.mwin)
        # actions.parseLinksToggle.setCheckable(True)
        # actions.parseLinksToggle.setChecked(True if self.sets.value('Textbox/ParseLinks', type=str).upper() == 'TRUE' else False)
        actions.parseLinksToggle.triggered.connect(self.action_parselinks_trig)
        # Debug
        actions.debugToggle = QAction('Debug', self.mwin)
        actions.debugToggle.setCheckable(True)
        actions.debugToggle.triggered.connect(self.action_debug_trig)
        # About
        actions.about = QAction('About', self.mwin)
        actions.about.triggered.connect(self.about_trig)
        # Language
        actions.language = QAction('*** Language ***', self.mwin)
        actions.language.triggered.connect(self.action_language_trig)
        # Quit
        actions.quit = QAction('Quit', self.mwin)
        actions.quit.triggered.connect(self.set_quiting)
        return actions

    def setup_context_menu(self):
        # Right-clk menu
        main_cntx_menu = QMenu('Main')
        main_cntx_menu.setIcon(self.app.windowIcon())

        main_cntx_menu.addAction(self.actions.hiddenToggle)
        main_cntx_menu.addAction(self.actions.autoHideToggle)
        main_cntx_menu.addAction(self.actions.alwaysOnTopToggle)
        main_cntx_menu.addAction(self.actions.wordWrapToggle)
        main_cntx_menu.addAction(self.actions.parseLinksToggle)
        if self.sets.contains('Debug'):  # if Debug is mentioned in settings, make able debug turn on/off
            main_cntx_menu.addAction(self.actions.debugToggle)
        main_cntx_menu.addAction(self.actions.about)
        main_cntx_menu.addSeparator()
        main_cntx_menu.addAction(self.actions.language)
        main_cntx_menu.addSeparator()
        main_cntx_menu.addAction(self.actions.quit)

        return main_cntx_menu

    def setup_tray_icon(self):
        tray_icon = QSystemTrayIcon()
        tray_icon.setIcon(self.app.windowIcon())
        tray_icon.setToolTip('{0} {1}'.format(info.program, info.version))
        tray_icon.activated.connect(self.tray_act)
        tray_icon.setContextMenu(self.main_cntx_menu)
        tray_icon.show()
        return tray_icon

    def setup_main_widget(self):
        wdg_main = QWidget(self.mwin)
        self.mwin.setCentralWidget(wdg_main)
        return wdg_main

    def setup_tab_widget(self):
        return QTabWidget(self.wdg_main)

    def setup_connect_debug(self):
        dbg = common.Capacitor()
        # Geometry temp
        dbg.lcapgeo = QLabel('[geometry]', self.wdg_main)
        dbg.lcapfgeo = QLabel('[frameGeometry]', self.wdg_main)
        dbg.lcapgeo.setMaximumWidth(300)
        dbg.lcapfgeo.setMaximumWidth(300)
        # Cursor pos temp
        dbg.lcapcurpos = QLabel('[cur pos]', self.wdg_main)
        dbg.lcapcurpos.setMaximumWidth(300)
        # Log box
        dbg.logprintnum = 0
        dbg.logbox = QPlainTextEdit(self.wdg_main)
        dbg.logbox.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOn)
        dbg.logbox.setReadOnly(True)
        dbg.logbox.setFocusPolicy(Qt.NoFocus)
        dbg.logbox.setFont(self.font_main)
        dbg.logbox.setMaximumHeight(200)
        return dbg

    def setup_tabs_controller(self):
        # Tabs controller
        tabctrl = tab_controller.TabsController(self.wdg_tab)
        for key in self.opts['Tabs'].keys():
            t_fontname = self.sets.value('Tabs/{0}/FontName'.format(key), type=str)
            t_fontsize = int(self.sets.value('Tabs/{0}/FontSize'.format(key), type=int))
            t_font = QFont(t_fontname, t_fontsize)
            t_wwrap = QTextOption.NoWrap  # TODO when *_trigs will be separated from *_sets, change to standard *_set
            if self.sets.value('Tabs/{0}/WordWrap'.format(key), type=str).upper() == 'TRUE':
                t_wwrap = QTextOption.WrapAtWordBoundaryOrAnywhere
            tabctrl.add_tab(key, t_font, t_wwrap)
        return tabctrl

    def setup_main_layout(self):
        # Main Layout
        wdg_main_lay = QGridLayout(self.wdg_main)
        wdg_main_lay.setContentsMargins(0, 0, 0, 0)
        wdg_main_lay.addWidget(self.wdg_tab, 0, 0, 1, 1)
        # Adding dbg
        if self.sets.contains('Debug'):
            wdg_main_lay.addWidget(self.dbg.lcapgeo, 1, 0, 1, 1)
            wdg_main_lay.addWidget(self.dbg.lcapfgeo, 2, 0, 1, 1)
            wdg_main_lay.addWidget(self.dbg.lcapcurpos, 3, 0, 1, 1)
            wdg_main_lay.addWidget(self.dbg.logbox, 4, 0, 1, 1)
        self.wdg_main.setLayout(wdg_main_lay)
        return wdg_main_lay

    def connect_txtbox(self, txtbox):
        # Links
        txtbox.anchorClicked.connect(self.anchor_link_act)
        # Parsing hyperlinks
        txtbox.document().contentsChange.connect(self.transform_hyperlinks)
        # Context menu
        txtbox.setContextMenuPolicy(Qt.CustomContextMenu)
        txtbox.customContextMenuRequested.connect(self.show_txtbox_menu)  # Method that execs txtboxMenu

    def retranslate_context_menu(self):
        self.main_cntx_menu.setTitle(tr.tr('Main'))
        self.actions.hiddenToggle.setText(tr.tr('Hidden'))
        self.actions.autoHideToggle.setText(tr.tr('Auto hide'))
        self.actions.alwaysOnTopToggle.setText(tr.tr('Always on top'))
        self.actions.wordWrapToggle.setText(tr.tr('Word wrap'))
        self.actions.parseLinksToggle.setText(tr.tr('Parse links'))
        if self.sets.contains('Debug'):
            self.actions.debugToggle.setText(tr.tr('Debug'))
        self.actions.about.setText(tr.tr("About"))
        self.actions.quit.setText(tr.tr('Quit'))

    # --------------------------------------
    # Slots
    # --------------------------------------
    def about_trig(self):
        QMessageBox.about(self.mwin, 'About {0}'.format(info.program), ''.join((str(x) for x in common.about)))
        self.m_show()

    def action_hidden_trig(self):
        if self.mwin.isVisible():
            self.m_hide()
        else:
            self.m_show()

    def action_autohide_trig(self):
        if self.sets.value('Win/AutoHide', type=str).upper() == 'TRUE':
            self.sets.setValue('Win/AutoHide', 'False')
            self.actions.autoHideToggle.setChecked(False)
        else:
            self.sets.setValue('Win/AutoHide', 'True')
            self.actions.autoHideToggle.setChecked(True)

    def action_alwaysontop_trig(self):
        if self.sets.value('Win/AlwaysOnTop', type=str).upper() == 'TRUE':
            self.mwin.setWindowFlags(self.mwin.windowFlags() & ~Qt.WindowStaysOnTopHint)
            self.sets.setValue('Win/AlwaysOnTop', 'False')
            self.actions.alwaysOnTopToggle.setChecked(False)
        else:
            self.mwin.setWindowFlags(self.mwin.windowFlags() | Qt.WindowStaysOnTopHint)
            self.sets.setValue('Win/AlwaysOnTop', 'True')
            self.actions.alwaysOnTopToggle.setChecked(True)
        self.mwin.show()

    def action_wordwrap_trig(self):
        key = self.tabctrl.get_key()
        if self.sets.value('Tabs/{0}/WordWrap'.format(key), type=str).upper() == 'TRUE':
            self.tabctrl.get_txtbox(key).setWordWrapMode(QTextOption.NoWrap)
            self.sets.setValue('Tabs/{0}/WordWrap'.format(key), 'False')
            # self.actions.wordWrapToggle.setChecked(False)
        else:
            self.tabctrl.get_txtbox(key).setWordWrapMode(QTextOption.WrapAtWordBoundaryOrAnywhere)
            self.sets.setValue('Tabs/{0}/WordWrap'.format(key), 'True')
            # self.actions.wordWrapToggle.setChecked(True)

    def action_parselinks_trig(self):
        key = self.tabctrl.get_key()
        if self.sets.value('Tabs/{0}/ParseLinks'.format(key), type=str).upper() == 'TRUE':
            self.sets.setValue('Tabs/{0}/ParseLinks'.format(key), 'False')
            # self.actions.parseLinksToggle.setChecked(False)
        else:
            self.sets.setValue('Tabs/{0}/ParseLinks'.format(key), 'True')
            # self.actions.parseLinksToggle.setChecked(True)
        # reformat whole text (it will parse or unparse links)
        self.save_cursor_position(key)
        self.tabctrl.get_txtbox(key).setCurrentCharFormat(self.tabctrl.get_tab(key).defaultCharFormat)
        self.tabctrl.get_txtbox(key).setText(self.tabctrl.get_txtbox(key).document().toPlainText())
        self.restore_cursor_position(key)

    def action_language_trig(self):
        if self.sets.value('Win/Language', type=str).upper() == 'RU':
            self.sets.setValue('Win/Language', 'EN')
            self.actions.language.setText('To Russian')
            self.app.removeTranslator(self.translator_ru)
        else:
            self.sets.setValue('Win/Language', 'RU')
            self.actions.language.setText('To English')
            self.app.installTranslator(self.translator_ru)
        self.retranslate_context_menu()

    def action_debug_trig(self):
        if self.sets.value('Debug', type=str).upper() == 'TRUE':
            self.dbg.logbox.hide()
            self.dbg.lcapgeo.hide()
            self.dbg.lcapfgeo.hide()
            self.dbg.lcapcurpos.hide()
            self.sets.setValue('Debug', 'False')
            self.actions.debugToggle.setChecked(False)
        else:
            self.dbg.logbox.show()
            self.dbg.lcapgeo.show()
            self.dbg.lcapfgeo.show()
            self.dbg.lcapcurpos.show()
            self.sets.setValue('Debug', 'True')
            self.actions.debugToggle.setChecked(True)

    # --------------------------------------
    def show_txtbox_menu(self, point):
        txtbox_menu = self.tabctrl.get_txtbox().createStandardContextMenu(point)
        txtbox_menu.insertMenu(txtbox_menu.actions()[0], self.main_cntx_menu)
        txtbox_menu.insertSeparator(txtbox_menu.actions()[1])
        txtbox_menu.exec_(self.tabctrl.get_txtbox().mapToGlobal(point))

    def paste(self):
        self.tabctrl.get_txtbox().insertPlainText('\n')
        self.tabctrl.get_txtbox().paste()

    def logprint(self, tstr, *args, **kwargs):
        if self.sets.contains('Debug'):
            self.dbg.logprintnum += 1
            tformatstr = str(tstr).format(*args, **kwargs)
            self.dbg.logbox.appendPlainText('{0:<4}: {1}'.format(self.dbg.logprintnum, tformatstr))

    def focuslot(self, twas, tnow):
        # TODO - simplify - start timer when app receives focus and stop it and save when looses
        if self.quiting:
            return
        # if textbox receives focus - starting autosaving timer
        if tnow == self.tabctrl.get_txtbox():
            if not self.mwin.timer:
                self.mwin.timer = self.mwin.startTimer(300000)
                self.logprint('TIMER STARTED')
        # if textbox lost focus - save and stop autosaving timer
        if twas == self.tabctrl.get_txtbox():
            self.save()
            if self.mwin.timer:
                self.mwin.killTimer(self.mwin.timer)
                self.mwin.timer = None
                self.logprint('TIMER STOPPED')
        # if application looses focus - hiding window (if option checked)
        if not tnow and self.sets.value('Win/AutoHide', type=str).upper() == 'TRUE':
            self.m_hide()

    def tray_act(self, arg):
        if arg == QSystemTrayIcon.Trigger:
            # Make window active and not minimized
            self.m_show_active()
        elif arg == QSystemTrayIcon.MiddleClick:
            # Just show/hide it w/o activation or restoring from minimized
            self.action_hidden_trig()

    def m_hide(self):
        self.mwin.hide()
        self.actions.hiddenToggle.setChecked(True)

    def m_show(self):
        self.mwin.show()
        self.actions.hiddenToggle.setChecked(False)

    def m_show_active(self):
        self.m_show()
        self.mwin.setWindowState(self.mwin.windowState() & ~Qt.WindowMinimized)
        self.mwin.activateWindow()

    def m_show_active_and_paste(self):
        self.m_show_active()
        self.paste()

    def dbg_show_stats(self):
        ag, fg = self.mwin.geometry(), self.mwin.frameGeometry()
        self.dbg.lcapgeo.setText('geometry(): {0},{1} | {2},{3}'.format(ag.x(), ag.y(), ag.width(), ag.height()))
        self.dbg.lcapfgeo.setText('frameGeometry(): {0},{1} | {2},{3}'.format(fg.x(), fg.y(), fg.width(), fg.height()))

    def dbg_show_cur_pos(self):
        cp = self.tabctrl.get_txtbox().textCursor().position()
        self.dbg.lcapcurpos.setText('cur pos: {0}'.format(cp))

    def save_geometry(self):
        self.sets.setValue('Win/Size', self.mwin.size())
        self.sets.setValue('Win/Pos', self.mwin.pos())

    def set_quiting(self):
        self.quiting = True
        self.logprint('QUITING')
        for key in self.tabctrl.get_keylist():
            self.save(key)  # JIC
        self.app.quit()  # not working .. but whatever)

    def tab_wheel(self):
        tabnum = self.tabctrl.tabholder.count()
        curtab = self.tabctrl.tabholder.currentIndex()
        if curtab < tabnum-1:
            curtab += 1
        else:
            curtab = 0
        self.tabctrl.tabholder.setCurrentIndex(curtab)

    @staticmethod
    def anchor_link_act(turl):
        QDesktopServices.openUrl(turl)

    def transform_hyperlinks(self, pos, rem, add):
        key = self.tabctrl.get_key()
        if self.sets.value('Tabs/{0}/ParseLinks'.format(key), type=str).upper() == 'TRUE':
            txt_cursor = QTextCursor(self.tabctrl.get_txtbox(key).document())

            href_class_format = QTextCharFormat()
            href_class_format.setForeground(Qt.darkBlue)
            href_class_format.setFontUnderline(True)

            curblock = self.tabctrl.get_txtbox(key).document().findBlock(pos)
            lastblock = self.tabctrl.get_txtbox(key).document().findBlock(pos + add)
            parsing = True

            while parsing:
                for m in self.opts['compRegExp'].finditer(curblock.text()):
                    txt_cursor.setPosition(curblock.position() + m.start())
                    txt_cursor.setPosition(curblock.position() + m.end(), QTextCursor.KeepAnchor)
                    href_class_format.setAnchorHref(m.string[m.start():m.end()])
                    txt_cursor.mergeCharFormat(href_class_format)
                if curblock == lastblock:
                    parsing = False
                else:
                    curblock = curblock.next()

    def save_cursor_position(self, key):
        txtbox = self.tabctrl.get_txtbox(key)
        if key is None:
            key = self.tabctrl.get_key()
        if txtbox is None:
            txtbox = self.tabctrl.get_txtbox()
        cp = txtbox.textCursor().position()
        self.sets.setValue('Tabs/{0}/CursorPos'.format(key), cp)

    def save_tab_index(self):
        self.sets.setValue('Win/FocusedTabKey', self.tabctrl.get_key())

    def restore_cursor_position(self, key):
        txtbox = self.tabctrl.get_txtbox(key)
        cur = txtbox.textCursor()
        saved_cur_pos = self.sets.value('Tabs/{0}/CursorPos'.format(key), type=int)
        cur.setPosition(int(saved_cur_pos))
        txtbox.setTextCursor(cur)
        # txtbox.centerOnScroll()  # Only for QPlainTextEdit
        txtbox.verticalScrollBar().triggerAction(QAbstractSlider.SliderToMaximum)
        txtbox.ensureCursorVisible()

    def save(self, key=None):
        if key is None:
            key = self.tabctrl.get_key()
        txtbox = self.tabctrl.get_txtbox(key)
        if txtbox.document().isModified():
            try:
                filename = self.opts['Tabs'][key]['FileNameFullPath']
                common.save_file(filename, txtbox.document().toPlainText())
                self.logprint('SAVED {0} TO {1}', len(txtbox.document().toPlainText()), filename)
            except common.SNException as e:
                self.logprint(e)
            txtbox.document().setModified(False)
        self.save_cursor_position(key)
        self.save_tab_index()

    def load(self, key):
        txtbox = self.tabctrl.get_txtbox(key)
        try:
            txtbox.setText(common.load_file(self.opts['Tabs'][key]['FileNameFullPath']))
        except common.SNException as e:
            self.logprint(e)
