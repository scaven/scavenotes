#!/usr/bin/env python3
# ------------------------------------------------------------------------------
# Scavenotes
# Scaven 2018 copyleft
# All lefts reserved
# ------------------------------------------------------------------------------

import info
info.version = "0.4.1"
info.program = "Scavenotes"
info.organization = 'Scaven Enterprises'

from frame import Mainframe
from ghotkeys.ghk import Listener


# ------------------------------------------------------------------------------
def main():
    m = Mainframe()
    m.logprint('App started')

    ln = Listener(m)
    ln.setup()

    m.app.exec_()


# ------------------------------------------------------------------------------
if __name__ == '__main__':
    main()
