import sys
import os
import codecs
import re
from collections import OrderedDict

from PyQt5.QtCore import QSettings, QSize, QPoint

import info
from tran.tr import *  # Translator


# ------------------------------------------------------------------------------
# Texts
# ------------------------------------------------------------------------------
about = (
    '<h1>{0} v{1}</h1><br>'.format(info.program, info.version),
    tr.tr(
        "Hotkeys: <br>"
        "Ctrl+~ - Show editor and focus on it <br>"
        "Ctrl+Win+Alt - Show editor without focus <br>"
        "Ctrl+Alt+C - Quick paste from buffer <br>"
        "<br>", lazy=True
    ),
    tr.tr(
        "Description: <br>"
        "<br>"
        "This is a text editor, designed for making quick notes. "
        "It has several useful features, such as: auto-hiding to tray, auto-saving"
        " and global hotkeys. "
        "When auto-hiding is enabled, the window of text editor automatically minimizes to"
        " tray when looses focus. It can be called by hotkey or by clicking on icon in"
        " tray. Text saves automatically when program looses focus, or every 5 minutes. "
        "I recommend to put it into autoload. "
        "<br>", lazy=True
    ),
    "<br><hr>"
    "Python: {python_version}; PyQt: {pylibqt_version}; Qt: {qt_version}<br>"
    "<br>"
    "Scaven copyleft 2018 all lefts reserved o_O"
    .format(
        python_version=info.python_version,
        pylibqt_version=info.pylibqt_version,
        qt_version=info.qt_version
    )
)


# ------------------------------------------------------------------------------
# Classes pile
# ------------------------------------------------------------------------------
class SNException(Exception):
    pass


class Capacitor(object):
    pass


class EmptyObject(object):
    def __call__(self):
        pass


EmptyObject.instance = EmptyObject()


# ------------------------------------------------------------------------------
# Functions pile
# ------------------------------------------------------------------------------
def make_opts():
    """ Fetching options | Making default set of options if not found """
    # Determining directory of program (cwd in _most_ cases)
    start_dir = os.path.abspath(os.path.dirname(sys.argv[0]))
    sets = QSettings(os.path.join(start_dir, 'scavenotes.ini'), QSettings.IniFormat)

    def _check_set(opt_name, opt_default_val):
        if not sets.contains(opt_name):
            sets.setValue(opt_name, opt_default_val)

    def _renorm_path(protopath):
        if os.path.isabs(protopath):
            return protopath
        return os.path.join(start_dir, os.path.dirname(protopath), os.path.basename(protopath))

    # Now going step-by-step settings checking
    _check_set('Win/AutoHide', 'True')
    _check_set('Win/AlwaysOnTop', 'True')
    _check_set('Win/Size', QSize(400, 600))
    _check_set('Win/Pos', QPoint(200, 200))
    _check_set('Win/Language', 'EN')
    _check_set('Win/FocusedTabKey', 'Main')

    # opts (options, that are not saved to ini)
    opts = dict(
        # regular expression for URL search (if ParseLinks is on)
        compRegExp=re.compile(r'''(?i)\b((?:[a-z][\w-]+:(?:/{1,3}|[a-z0-9%])|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:'".,<>?«»“”‘’]))''')
    )

    # Temporary tabs dict
    tabs = {}
    order = 0
    sets.beginGroup('Tabs')
    if not len(sets.childGroups()):
        sets.setValue('Main/Filename', 'scavenotes.txt')
        sets.setValue('Main/Order', 0)
        sets.setValue('Subs/Filename', 'scavenotes_subs.txt')
        sets.setValue('Subs/Order', 1)
        sets.setValue('Trix/Filename', 'scavenotes_trix.txt')
        sets.setValue('Trix/Order', 2)
    for groupCur in sets.childGroups():
        _check_set(groupCur+'/FileName', '{0}.txt'.format(groupCur))
        _check_set(groupCur+'/FontName', 'Courier New')
        _check_set(groupCur+'/FontSize', 10)
        _check_set(groupCur+'/CursorPos', 0)
        _check_set(groupCur+'/WordWrap', 'True')
        _check_set(groupCur+'/ParseLinks', 'False')
        _check_set(groupCur+'/Order', order)
        order = int(sets.value(groupCur+'/Order', type=int))  # it`s ok if there are many items with same order value
        tabs[groupCur] = {}
        tabs[groupCur]['FileNameFullPath'] = _renorm_path(str(sets.value(groupCur+'/FileName', type=str)))
        tabs[groupCur]['order'] = order
        order += 1
    sets.endGroup()
    # Transferring tabs dict to ordered dict
    opts['Tabs'] = OrderedDict(sorted(tabs.items(), key=lambda t: t[1]['order']))

    return sets, opts


def save_file(filepath, text):
    try:
        with codecs.open(filepath, 'wb', 'utf_8_sig') as f:
            f.write(str(text))
    except IOError:
        raise SNException('ERROR: Write failure: {0}'.format(filepath))


def load_file(filepath):
    if os.path.isfile(filepath):
        try:
            with codecs.open(filepath, 'rb', 'utf_8_sig') as f:
                return f.read()
        except IOError:
            raise SNException('ERROR: Read failure: {0}'.format(filepath))
    else:
        raise SNException('Creating new: {0}'.format(filepath))


# ------------------------------------------------------------------------------
if __name__ == "__main__":
    from pprint import pprint
    tsets, topts = make_opts()
    pprint(topts)
    print(tsets)
    print(tsets.value('Win/Language', type=str))
    if tsets.value('Win/Language', type=str).upper() == 'EN':
        print('OLOLO')
    if tsets.value('Win/AutoHide', type=str).upper() == 'TRUE':
        print('URURU')
