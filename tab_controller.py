from PyQt5.QtCore import Qt
from PyQt5.QtGui import QFont, QTextOption, QFontMetrics
from PyQt5.QtWidgets import QApplication, QTabWidget, QWidget, QTextBrowser, QGridLayout

from common import SNException


class SNTabsException(SNException):
    pass


class TabsController:
    def __init__(self, tabholder_widget):
        self.tabs = {}
        self.tabholder = tabholder_widget

    def add_tab(
        self,
        key,
        textbox_font=QFont('Courier New', 10),
        wordwrap_mode=QTextOption.WrapAtWordBoundaryOrAnywhere
    ):
        if key not in self.tabs:
            ntab = QWidget()
            ntab.key = key
            self.tabs[key] = ntab
            self.tabholder.addTab(ntab, key)  # key == title for now
            # Txtbox
            self._create_txtbox(ntab, textbox_font, wordwrap_mode)
        else:
            raise SNException('ERROR: Tab with key "{0}" already exists'.format(key))

    @staticmethod
    def _create_txtbox(holdertab, textbox_font, wordwrap_mode):
        # Text editor
        txtbox = QTextBrowser(holdertab)
        holdertab.txtbox = txtbox
        txtbox.setOpenLinks(False)
        txtbox.setUndoRedoEnabled(True)
        # Options
        txtbox.setFocusPolicy(Qt.WheelFocus)
        txtbox.setTextInteractionFlags(Qt.TextEditorInteraction | Qt.TextEditable | Qt.LinksAccessibleByMouse)
        txtbox.setAcceptRichText(False)
        txtbox.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOn)
        txtbox.setWordWrapMode(wordwrap_mode)
        # Font
        txtbox.setFont(textbox_font)
        txtbox.setTabStopWidth(QFontMetrics(textbox_font).width(' ') * 4)  # Making tab char width = 4 spaces
        holdertab.defaultCharFormat = txtbox.currentCharFormat()
        # Layout
        tab_lay = QGridLayout(holdertab)
        tab_lay.setContentsMargins(0, 0, 0, 0)
        tab_lay.addWidget(txtbox, 0, 0, 1, 1)
        holdertab.setLayout(tab_lay)
        holdertab.tabLay = tab_lay

    def get_tab(self, key=None):
        if key is None:
            return self.tabholder.currentWidget()
        if key in self.tabs:
            return self.tabs[key]
        else:
            raise SNTabsException("No tab with key {}".format(key))

    def get_txtbox(self, key=None):
        return self.get_tab(key).txtbox

    def get_key(self):
        return self.get_tab().key

    def get_keylist(self):
        return (key for key in self.tabs.keys())


# ------------------------------------------------------------------------------
if __name__ == "__main__":
    import sys
    app = QApplication(sys.argv)
    # app.focusChanged.connect(lambda twas, tnow: print('FOCUS: {0} -> {1}'.format(twas, tnow)))
    test_tabholder = QTabWidget()
    test_tabc = TabsController(test_tabholder)
    test_tabc.add_tab('TEST 1')
    test_tabc.add_tab('TEST 2', textbox_font=QFont('Verdana', 18))
    test_tabc.get_txtbox().setFocus()
    test_tabholder.show()
    sys.exit(app.exec_())
