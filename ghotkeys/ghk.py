import platform

from PyQt5.QtCore import QTimer, QObject, pyqtSignal


class Listener(object):
    def __init__(self, mainframeobj):
        self.m = mainframeobj

    def setup(self):
        system_name = platform.system()
        if system_name == 'Linux':
            self._do_linux()
        elif system_name == 'Windows':
            self._do_windows()

    def _do_windows(self):
        self.m.logprint('Hello Windows')

        import ctypes
        from ctypes.wintypes import MSG
        import win32con

        # Registering some Hotkey Combos
        if not ctypes.windll.user32.RegisterHotKey(None, 1, win32con.MOD_CONTROL, 192):  # '~' key
            self.m.logprint("Unable to register id 1")
        if not ctypes.windll.user32.RegisterHotKey(None, 2, win32con.MOD_CONTROL | win32con.MOD_WIN | win32con.MOD_ALT, None):
            self.m.logprint("Unable to register id 2")
        if not ctypes.windll.user32.RegisterHotKey(None, 3, win32con.MOD_CONTROL | win32con.MOD_ALT, 67):  # 'C'
            self.m.logprint("Unable to register id 3")

        msg = MSG()
        # Making an event loop instead of ordinary app.exec_ (hack for hotkeyz)
        # When going to quit - break the loop and go to real app.exec_, so that qt makes all its stuff
        while not self.m.quiting:
            ctypes.windll.user32.GetMessageA(ctypes.byref(msg), None, 0, 0)
            if msg.message == win32con.WM_HOTKEY:  # Hotkey message recieved
                # msg.wParam - id of hotkey
                if msg.wParam == 1:
                    self.m.m_show_active()
                elif msg.wParam == 2:
                    self.m.action_hidden_trig()
                elif msg.wParam == 3:
                    self.m.m_show_active_and_paste()
            # I don`t know how it can happen here (CAD termination mb?), but in case
            if msg.message == win32con.WM_QUIT or msg.message == win32con.WM_DESTROY:
                self.m.set_quiting()  # saving and stuff

            ctypes.windll.user32.TranslateMessage(ctypes.byref(msg))
            ctypes.windll.user32.DispatchMessageA(ctypes.byref(msg))

        # let the app quit through the cycle of exec_ -> quit
        # make a short one-shot timer that triggers quit event in 0.1 second
        QTimer.singleShot(100, self.m.app.quit)
        ctypes.windll.user32.UnregisterHotKey(None, 1)
        ctypes.windll.user32.UnregisterHotKey(None, 2)
        ctypes.windll.user32.UnregisterHotKey(None, 2)

    def _do_linux(self):
        self.m.logprint('Hello Linux')

        import ghk_xlib

        class XlibGlobalHotkeyListener(QObject):

            global_hotkey1 = pyqtSignal()
            global_hotkey2 = pyqtSignal()
            global_hotkey3 = pyqtSignal()

            def __init__(self, parent=None):
                QObject.__init__(self, parent)
                self.startTimer(300)
                ghk_xlib.init()
                ghk_xlib.callback_dict = {1: self.global_hotkey1.emit, 2: self.global_hotkey2.emit, 3: self.global_hotkey3.emit}

            def timerEvent(self, event):
                ghk_xlib.check_nonblocking()

        self.xghl = XlibGlobalHotkeyListener()
        self.xghl.global_hotkey1.connect(self.m.m_show_active)
        self.xghl.global_hotkey2.connect(self.m.action_hidden_trig)
        self.xghl.global_hotkey3.connect(self.m.m_show_active_and_paste)

