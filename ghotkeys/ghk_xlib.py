#!/usr/bin/env python3

from Xlib import X, XK
from Xlib.display import Display

disp = Display()
root = disp.screen().root

callback_dict = {1: lambda: None, 2: lambda: None, 3: lambda: None}  # dummies


def _grab(key_code, mod):
    # 3rd: bool owner_events, 4th: pointer_mode, 5th: keyboard_mode
    # mod1 - alt(meta), mod2 - num lock, mod3 - alt gr (?), mod4 - win key, mod5 - scroll lock
    root.grab_key(key_code, mod, 0, X.GrabModeAsync, X.GrabModeAsync)
    root.grab_key(key_code, mod | X.LockMask, 0, X.GrabModeAsync, X.GrabModeAsync)  # caps lock
    root.grab_key(key_code, mod | X.Mod2Mask, 0, X.GrabModeAsync, X.GrabModeAsync)  # num lock
    root.grab_key(key_code, mod | X.LockMask | X.Mod2Mask, 0, X.GrabModeAsync, X.GrabModeAsync)  # caps + num lock


def _handle(evt):
    key_code = evt._data['detail']
    if key_code == 49:
        callback_dict[1]()
    elif key_code == 64:
        callback_dict[2]()
    elif key_code == 54:
        callback_dict[3]()


def init():
    # key strings - google X11/keysymdef.h
    # Ctrl + tilde
    key_code = disp.keysym_to_keycode(XK.string_to_keysym('asciitilde'))  # 49
    _grab(key_code, X.ControlMask)  # X.NONE

    # Ctrl + Alt + Win
    key_code = disp.keysym_to_keycode(XK.string_to_keysym('Alt_L'))  # 64
    _grab(key_code, X.ControlMask | X.Mod4Mask)

    # Ctrl + Alt + C
    key_code = disp.keysym_to_keycode(XK.string_to_keysym('c'))  # 54
    _grab(key_code, X.ControlMask | X.Mod1Mask)


def check_nonblocking():
    evt_cnt = root.display.pending_events()
    for i in range(evt_cnt):
        evt = root.display.next_event()
        if evt.type in [X.KeyRelease]:  # X.KeyPress
            _handle(evt)


# ------------------------------------------------------------------------------
if __name__ == '__main__':
    import sys
    import time

    class Test:
        counter = 0

        @classmethod
        def h1(cls):
            print(' Ctrl + tilde ')
            cls.count()

        @classmethod
        def h2(cls):
            print(' Ctrl + Alt + Win ')
            cls.count()

        @classmethod
        def h3(cls):
            print(' Ctrl + Alt + C ')
            cls.count()

        @classmethod
        def count(cls):
            cls.counter += 1
            if cls.counter > 4:
                sys.exit(0)

    callback_dict = {1: Test.h1, 2: Test.h2, 3: Test.h3}

    init()
    while 1:
        check_nonblocking()
        time.sleep(0.2)  # half a sec
