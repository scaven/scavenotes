""" Holds all the needed info about version of this program and its components """

# Python version
from platform import python_version as __python_version

# Lib versions
from PyQt5.QtCore import PYQT_VERSION_STR as pylibqt_version
from PyQt5.QtCore import QT_VERSION_STR as qt_version

# Program version and name are set from within main
program = ''
version = ''
organization = ''

# Python version
python_version = __python_version()
