<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="ru">
<context>
    <name>@default</name>
    <message>
        <location filename="../scavenotes.pyw" line="264"/>
        <source>Hidden</source>
        <translation>Скрыт</translation>
    </message>
    <message>
        <location filename="../scavenotes.pyw" line="265"/>
        <source>Auto hide</source>
        <translation>Скрывать автоматически</translation>
    </message>
    <message>
        <location filename="../scavenotes.pyw" line="266"/>
        <source>Always on top</source>
        <translation>Перекрывать другие окна</translation>
    </message>
    <message>
        <location filename="../scavenotes.pyw" line="267"/>
        <source>Word wrap</source>
        <translation>Переносить слова</translation>
    </message>
    <message>
        <location filename="../scavenotes.pyw" line="268"/>
        <source>Parse links</source>
        <translation>Разбор ссылок</translation>
    </message>
    <message>
        <location filename="tr.py" line="106"/>
        <source>About</source>
        <translation>О программе</translation>
    </message>
    <message>
        <location filename="../scavenotes.pyw" line="273"/>
        <source>Quit</source>
        <translation>Выход</translation>
    </message>
    <message>
        <location filename="../scavenotes.pyw" line="270"/>
        <source>Debug</source>
        <translation>Debug</translation>
    </message>
    <message>
        <location filename="../scavenotes.pyw" line="263"/>
        <source>Main</source>
        <translation>Главное меню</translation>
    </message>
    <message>
        <location filename="../common.py" line="28"/>
        <source>Hotkeys: &lt;br&gt;Ctrl+~ - Show editor and focus on it &lt;br&gt;Ctrl+Win+Alt - Show editor without focus &lt;br&gt;Ctrl+Alt+C - Quick paste from buffer &lt;br&gt;&lt;br&gt;</source>
        <translation>Горячие клавиши: &lt;br&gt;Ctrl+~ - Вызвать редактор и перевести на него фокус &lt;br&gt;Ctrl+Win+Alt - Вызвать редактор без перевода фокуса &lt;br&gt;Ctrl+Alt+C - Быстрая вставка из буфера &lt;br&gt;&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="../common.py" line="41"/>
        <source>Description: &lt;br&gt;&lt;br&gt;This is a text editor, designed for making quick notes. It has several useful features, such as: auto-hiding to tray, auto-saving and global hotkeys. When auto-hiding is enabled, the window of text editor automatically minimizes to tray when looses focus. It can be called by hotkey or by clicking on icon in tray. Text saves automatically when program looses focus, or every 5 minutes. I recommend to put it into autoload. &lt;br&gt;</source>
        <translation>Описание: &lt;br&gt;&lt;br&gt;Это текстовый редактор, предназначенный для создания быстрых заметок. В нем есть несколько полезных функций, таких как: автоматическое скрытие в трей, автосохранение и глобальные горячие клавиши. Когда автоматическое скрытие включено, при потере фокуса окно редактора автоматически сворачивается в трей. Его можно восстановить по горячей клавише или нажатем на иконку в трее. Текст сохраняется автоматически, когда программа теряет фокус, или каждые 5 минут. Рекомендую положить его в автозагрузку.&lt;br&gt;</translation>
    </message>
</context>
<context>
    <name>test</name>
    <message>
        <location filename="tr.py" line="112"/>
        <source>&amp;&amp;&amp;TEST&amp;&amp;&amp;</source>
        <translation>&amp;&amp;&amp;ТЕСТ&amp;&amp;&amp;</translation>
    </message>
    <message>
        <location filename="tr.py" line="116"/>
        <source>ANOTHER TEST OF LAZY TRANSLATION!</source>
        <translation>ДРУГОЙ ТЕСТ ЛЕНИВОГО ПЕРЕВОДА!</translation>
    </message>
</context>
<context>
    <name>test.lazy</name>
    <message>
        <location filename="tr.py" line="96"/>
        <source>TESTING TRANSLATION! THIS IS A TEST</source>
        <translation>ТЕСТИРУЕТСЯ ПЕРЕВОД! ЭТО ТЕСТ</translation>
    </message>
</context>
</TS>
