"""
Code usage :
    from tr import tr
    t = tr.tr("Something to translate") # context: @default
    l1 = tr.lazy.tr("Something to translate on evaluation") # context: lazy
    l2 = tr.tr("Something more to translate on evaluation", lazy=True) # context: @default

    from tr import tr_custom
    t = trCustom.tr("Something to translate") # context: trCustom
    l1 = trCustom.lazy.tr("Something to translate on evaluation") # context: trCustom.lazy
    l2 = trCustom.tr("Something more to translate on evaluation", lazy=True) # context: trCustom

Utils cheat sheat:
    Parsing sources and creating .ts files:
        [Path to pyside]/pyside-lupdate scavenotes.pro
    Then open .ts file in QtLinguist and make translations
    Compile .ts to .qm for using in program:
        [Path to pyside]/lrelease.exe scavenotes.pro -nounfinished -compress -removeidentical -markuntranslated ?TR?
    Remove obsolete translations:
        [Path to pyside]/lconvert.exe -i tran_scavenotes_ru.ts -o tran_scavenotes_ru.ts --no-obsolete
"""

from PyQt5 import QtCore, QtGui, QtWidgets


class _LazyTran(object):
    """ Object that stores string inside and performs translation """

    def __init__(self, strn, translator=None, context=None):
        self.strn = strn
        self.translator = translator
        self.context = context

    def __str__(self):
        return self.translator.tr(self.strn, context=self.context)


class _LazyFactory(object):
    """ This produces _LazyTran objects on tr() method """

    def __init__(self, parent, context=None):
        self.parent = parent
        self.context = context

    def tr(self, strn):
        return _LazyTran(strn, translator=self.parent, context=self.context)


class TranHack(object):
    """
        Translator class
        Context is a string, that must be the same as the name of the variable
    """

    def __init__(self, context):
        self.context = context
        self.lazy = _LazyFactory(self, context+'.lazy')

    @staticmethod
    def correct_context(context_str):
        context_str = context_str.strip()
        context_str = context_str.replace('tr', '')
        while '..' in context_str:
            context_str = context_str.replace('..', '.')
        context_str = context_str.strip('.')
        if not context_str:
            context_str = '@default'
        return context_str

    def __call__(self, strn):
        self.tr(strn)

    def tr(self, strn, lazy=False, context=None):
        if not lazy:
            if not context:
                context = self.context
            context = self.correct_context(context)
            trstr = QtCore.QCoreApplication.translate(context, strn)  # .encode('utf-8'))
            return trstr
        else:
            return _LazyTran(strn, translator=self)


# Main translator
tr = TranHack('tr')

# Custom translators - Add here
pass


# ------------------------------------------------------------------------------
if __name__ == '__main__':

    # Testing main lazy translation before setting up the App
    tr_main_lazy = tr.tr('About', lazy=True)

    # Testing creation of custom context translator and it`s lazy translation
    test = TranHack('test')
    tr_custom_lazy = test.lazy.tr('TESTING TRANSLATION! THIS IS A TEST')
    tr_custom_lazy2 = test.tr('ANOTHER TEST OF LAZY TRANSLATION!', lazy=True)

    # Setting App
    app = QtWidgets.QApplication([])
    test_translator = QtCore.QTranslator()
    test_translator.load("tran_scavenotes_ru", "")
    app.installTranslator(test_translator)

    # Main translator
    print('TEST MAIN TRANSLATOR: ')
    print('{0} -> {1}'.format('About', tr.tr('About')))
    print('TEST MAIN LAZY TRANSLATOR: ')
    print('{0}'.format(tr_main_lazy))

    # Custom translator
    print('TEST CUSTOM TRANSLATOR: ')
    print('{0} -> {1}'.format('&&&TEST&&&', test.tr('&&&TEST&&&')))
    print('TEST CUSTOM LAZY TRANSLATOR: ')
    print('{0}; {1}'.format(tr_custom_lazy, tr_custom_lazy2))
    print('{0}'.format(test.tr('ANOTHER TEST OF LAZY TRANSLATION!', lazy=True)))
    print(test.tr('ANOTHER TEST OF LAZY TRANSLATION!', lazy=True))
